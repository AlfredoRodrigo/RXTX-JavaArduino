int casa1 = 0;
int casa2 = 0;
int casa3 = 0;
int casa4 = 0;
int casa5 = 0;
int casa6 = 0;
int casa7 = 0;
int casa8 = 0;
int casa9 = 0;
int casa10 = 0;

String separador = ",";

void setup() {
  Serial.begin(9600);
  pinMode(3, OUTPUT);
  digitalWrite(3, HIGH);
}

void loop() {
  delay(500);
  
  casa1 = digitalRead(6);
  casa2 = digitalRead(7);
  casa3 = digitalRead(8);
  casa4 = digitalRead(9);
  casa5 = digitalRead(10);
  casa6 = digitalRead(11);
  casa7 = digitalRead(12);
  casa8 = digitalRead(13);

  enviaDadosSerial();
}

void enviaDadosSerial() {
  if (casa1) {
    Serial.print("1");
  }
  else {
    Serial.print("-1");
  }
  Serial.print(separador);
  if (casa2) {
    Serial.print("2");
  }
  else {
    Serial.print("-2");
  }
  Serial.print(separador);
  if (casa3) {
    Serial.print("3");
  }
  else {
    Serial.print("-3");
  }
  Serial.print(separador);
  if (casa4) {
    Serial.print("4");
  }
  else {
    Serial.print("-4");
  }
  Serial.print(separador);
  if (casa5) {
    Serial.print("5");
  }
  else {
    Serial.print("-5");
  }
  Serial.print(separador);
  if (casa6) {
    Serial.print("6");
  }
  else {
    Serial.print("-6");
  }
  Serial.print(separador);
  if (casa7) {
    Serial.print("7");
  }
  else {
    Serial.print("-7");
  }
  Serial.print(separador);
  if (casa8) {
    Serial.print("8");
  }
  else {
    Serial.print("-8");
  }
  Serial.print("\n");
}

