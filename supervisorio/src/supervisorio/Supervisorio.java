/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supervisorio;

import Serial.SerialRxTx;

/**
 *
 * @author alf_r - extraído do professor André Curvello: https://www.youtube.com/channel/UCH-yNyoZficULIjce_2OzvA
 */
public class Supervisorio implements Runnable {
    public void run() {
        SerialRxTx serial = new SerialRxTx();
        if (serial.iniciaSerial()) {
            while (true) {                
                System.out.print(serial.getProtocolo().getCasa1() + " ");
                System.out.print(serial.getProtocolo().getCasa2() + " ");
                System.out.print(serial.getProtocolo().getCasa3() + " ");
                System.out.print(serial.getProtocolo().getCasa4() + " ");
                System.out.print(serial.getProtocolo().getCasa5() + " ");
                System.out.print(serial.getProtocolo().getCasa6() + " ");
                System.out.print(serial.getProtocolo().getCasa7() + " ");
                System.out.print(serial.getProtocolo().getCasa8() + "\n");
            }
        } else {
            
        }
    }
    
}
